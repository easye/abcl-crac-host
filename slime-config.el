(setq slime-lisp-implementations
      '((abcl ("/home/alejandrozf/projects/abcl/abcl"))
        ;; j21crac == /usr/lib/jvm/zulu-21-crac-amd64/bin/java
        (crac ("/usr/lib/jvm/zulu-21-crac-amd64/bin/java" "-XX:CRaCRestoreFrom=/home/alejandrozf/external-projects/abcl-zulu-crac/crac-image2"))
        (sbcl ("sbcl"))
        (clisp ("clisp" "-ansi"))
        (ecl ("ecl"))
        (ccl ("/home/alejandrozf/Downloads/linuxx86/lx86cl64"))))
